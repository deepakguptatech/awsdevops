Create IAM Role: rol-s3
S3FullAccess

Create ServiceLinkRole For CodeDeploy to deploy on ec2 instances.
rol-cdservice

Launch ec2 instance with Amazon Linux 2
Attach rol-s3 to the instance
Add Tags: 
App: webserver

Go to IAM and add a user
appuser
Provide AWSCodeDeployFullAccess
S3FullAccess

aws configure with user_id
# Patching of the server is optional, check you application requirement or ask your architect

#sudo yum update -y

sudo yum -y install ruby wget

cd /home/ec2-user

wget https://aws-codedeploy-us-east-1.s3.amazonaws.com/latest/install

chmod +x ./install
sudo ./install auto
sudo service codedeploy-agent status

aws deploy create-application --application-name webserver
aws deploy push --application-name webserver --s3-location s3://cd-israr/webapp.zip --ignore-hidden-files

To deploy with revision number
aws deploy create-deployment --application-name webserver --s3-location bucket=cd-israr,key=flask.zip,bundleType=zip,eTag=8ca2315839704e9ca0b0cfceb5c327d0,version=iG6WYqiFy9Z..ighc7alNKkbl3E1jtlL --deployment-group-name <deployment-group-name> --deployment-config-name <deployment-config-name> --description <description>


